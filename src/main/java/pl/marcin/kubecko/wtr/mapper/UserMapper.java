package pl.marcin.kubecko.wtr.mapper;

import pl.marcin.kubecko.wtr.dto.UserDTO;
import pl.marcin.kubecko.wtr.entity.UserEntity;

public class UserMapper {

    public static UserEntity mapToUser(final UserDTO userDTO) {
        return new UserEntity(
                userDTO.getId(),
                userDTO.getLogin(),
                userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getRole(),
                userDTO.getPassword());
    }

    public static UserDTO mapToUserDTO(final UserEntity user) {
        return new UserDTO(
                user.getId(),
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getRole(),
                user.getPassword());
    }
}
