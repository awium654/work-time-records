package pl.marcin.kubecko.wtr.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.marcin.kubecko.wtr.dto.ResetPasswordDTO;
import pl.marcin.kubecko.wtr.dto.UserDTO;
import pl.marcin.kubecko.wtr.entity.UserEntity;
import pl.marcin.kubecko.wtr.mapper.UserMapper;
import pl.marcin.kubecko.wtr.repository.UserRepository;
import pl.marcin.kubecko.wtr.repository.WorkCardRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class UserController {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WorkCardRepository workCardRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/getUsers")
    public String getUsers(Model model) {
        List<UserDTO> findAllUserDTO = userRepository.findAll().stream().map(UserMapper::mapToUserDTO)
                .collect(Collectors.toList());
        model.addAttribute("allUsers",findAllUserDTO);
        return "viewAllUsers";
    }

    @PatchMapping("/")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO) {

        Optional<UserEntity> employee = userRepository.findById(userDTO.getId());
        if (employee.isPresent()) {

            UserEntity userToSave = employee.get();

            userToSave.setPassword(userDTO.getPassword());
            userToSave.setLastName(userDTO.getLastName());
            userToSave.setFirstName(userDTO.getFirstName());
            UserEntity saved = userRepository.save(userToSave);
            UserDTO mapperUserDTO = UserMapper.mapToUserDTO(saved);

            return ResponseEntity.status(HttpStatus.CREATED).body(mapperUserDTO);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/delete/{userId}")
    @GetMapping("/delete/{userId}")
    public String deleteUser(@PathVariable Integer userId, Model model) {
        Optional<UserEntity> user = userRepository.findById(userId);

        if (user.isPresent()) {
            UserEntity deleteUser = user.get();
            userRepository.delete(deleteUser);
        }
        List<UserDTO> findAllUserDTO = userRepository.findAll().stream().map(UserMapper::mapToUserDTO)
                .collect(Collectors.toList());
        model.addAttribute("allUsers",findAllUserDTO);
            return "viewAllUsersDelete";
    }

    @GetMapping("/changePassword/{userId}")
    public String getChangePassword(@RequestParam(required = false) boolean changePasswordIsFalse, Model model) {
        model.addAttribute("changePasswordIsFalse", changePasswordIsFalse);
        logger.info("open change password");
        return "changePassword";
    }

    @PostMapping("/changePassword/{userId}")
    public String changePassword(@PathVariable String userId, @ModelAttribute ResetPasswordDTO resetPasswordDTO) {

        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userUsername = user.getUsername();
        Optional<UserEntity> userFind = userRepository.findByLogin(userUsername);

        if (userFind.isPresent()) {
            UserEntity optionalUser = userFind.get();

            if (passwordEncoder.matches(resetPasswordDTO.getOldPassword(), optionalUser.getPassword())
                    && !resetPasswordDTO.getNewPassword().equals(resetPasswordDTO.getOldPassword())) {

                user.setPassword(passwordEncoder.encode(resetPasswordDTO.getNewPassword()));
                userRepository.save(user);
                logger.info("password is changed");
                return "redirect:/login?changePasswordIsTrue=true";
            }
        }
        return "redirect:/changePassword/{userId}?changePasswordIsFalse=true";
    }

    @GetMapping("/register")
    public String getRegisterPage(@RequestParam(required = false) boolean loginExist,
                                  @RequestParam(required = false) boolean created,
                                  Model model) {
        model.addAttribute("loginExist", loginExist);
        model.addAttribute("created",created);
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute UserDTO userDTO) {
        Optional<UserEntity> findLogin = userRepository.findByLogin(userDTO.getLogin());
        if (findLogin.isPresent()) {
            return "redirect:/register?loginExist=true";
        } else {
            UserEntity userSave = UserMapper.mapToUser(userDTO);
            userSave.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            UserEntity userSaveDb = userRepository.save(userSave);
            UserMapper.mapToUserDTO(userSaveDb);
        }
        logger.info("open register in userController");
        return "redirect:/register?created=true";
    }
}