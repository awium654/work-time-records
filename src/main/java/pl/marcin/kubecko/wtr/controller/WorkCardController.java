package pl.marcin.kubecko.wtr.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.marcin.kubecko.wtr.dto.WorkCardDTO;
import pl.marcin.kubecko.wtr.entity.UserEntity;
import pl.marcin.kubecko.wtr.entity.WorkCard;
import pl.marcin.kubecko.wtr.mapper.WorkCardMapper;
import pl.marcin.kubecko.wtr.repository.UserRepository;
import pl.marcin.kubecko.wtr.repository.WorkCardRepository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("card")
public class WorkCardController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WorkCardRepository workCardRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserController userController;

    @GetMapping("/panelUser")
    public String panelUser(Model model) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("user", user);
        logger.info("open PANEL USER in WorkCardController");
        return "panel-user";
    }

    @GetMapping("/createPosition")
    public String createCardUser() {
        logger.info("open add card position");
        return "add-card-position";
    }

    @PostMapping("/createPosition")
    public String createCardPostUser(@ModelAttribute WorkCard workCard) {
        Integer userId = 0;
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        Optional<UserEntity> findUserName = userRepository.findByLogin(username);

        if (findUserName.isPresent()) {
            UserEntity userExist = findUserName.get();
            userId = userExist.getId();
        }

        workCard.setDate_and_Time(OffsetDateTime.now());
        workCard.setUserId(userId);
        WorkCardDTO workCardDTOToSave = WorkCardMapper.mapToWorkCardDTO(workCard);
        WorkCard workCardToSave = WorkCardMapper.mapToWorkCard(workCardDTOToSave);

        workCardRepository.save(workCardToSave);
        logger.info("add position success");
        return "redirect:/card/getEmployeeCard/" + userId + "?created=true";
    }

    @GetMapping("/getAllWorkCards")
    public ResponseEntity<?> getEmployeeWorkCards() {
        List<WorkCardDTO> workCardList = workCardRepository.findAll().stream().map(WorkCardMapper::mapToWorkCardDTO)
                .collect(Collectors.toList());
        return ResponseEntity.ok(workCardList);
    }

    @GetMapping("/getEmployeeCard/{userId}")
    @PostMapping("/getEmployeeCard/{userId}")
    public String getEmployeeCard(@PathVariable String userId, Model model,
                                  @RequestParam(required = false) boolean created) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        Optional<UserEntity> findUserName = userRepository.findByLogin(username);
        Integer userIdInt = 0;
        if (findUserName.isPresent()) {
            UserEntity userExist = findUserName.get();
            userIdInt = userExist.getId();
        }
        List<WorkCard> workCards = workCardRepository.findByUserId(userIdInt);
        userId = String.valueOf(userIdInt);

        model.addAttribute("workList", workCards);
        model.addAttribute("username", "Hello," + username.toUpperCase());
        model.addAttribute("created", created);
        model.addAttribute("user", user);
        return "panel-user";
    }

    @GetMapping("/searching/{userId}")
    public String getEmployeeCard(@PathVariable String userId, Model model,
                                  @RequestParam(required = false) String criteria) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        Optional<UserEntity> findUserName = userRepository.findByLogin(username);
        Integer userIdInt = 0;
        if (findUserName.isPresent()) {
            UserEntity userExist = findUserName.get();
            userIdInt = userExist.getId();
        }

        if (criteria != null) {
            List<WorkCard> workCards = workCardRepository.findByUserIdAndNameWorkContainsIgnoreCaseOrUserIdAndNameActionContainsIgnoreCase(userIdInt, criteria, userIdInt, criteria);
            model.addAttribute("workList", workCards);
            model.addAttribute("user", user);

            return "panel-user";
        } else {
            List<WorkCard> workCards = workCardRepository.findByUserId(userIdInt);
            userId = String.valueOf(userIdInt);
            model.addAttribute("workList", workCards);
            model.addAttribute("user", user);
            return "panel-user";
        }
    }

    @DeleteMapping("/deletePositionFromWorkCard/{id}/{userId}")
    public ResponseEntity<?> deletePositionFromWorkCard(@PathVariable Integer id, @PathVariable Integer userId) {
        Optional<WorkCard> workCardFind = workCardRepository.findByIdAndUserId(id, userId);

        if (workCardFind.isPresent()) {
            WorkCard workCardDelete = workCardFind.get();
            workCardRepository.delete(workCardDelete);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found nr userId or id card work!");
        }
    }
}