package pl.marcin.kubecko.wtr.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.marcin.kubecko.wtr.dto.Mail;
import pl.marcin.kubecko.wtr.service.SimpleEmailService;

@Controller
public class MailController {
    public Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private SimpleEmailService simpleEmailService;

    @RequestMapping("/sendMail")
    public String sendMail(@ModelAttribute Mail mail) {
        simpleEmailService.send(mail);
        return "login";
    }

    @GetMapping("/sendMail")
    public String sendMail() {
        logger.info("open email");
        return "mail";
    }
}
