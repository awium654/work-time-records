package pl.marcin.kubecko.wtr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import pl.marcin.kubecko.wtr.entity.UserEntity;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    UserDetails getUserByLogin(String username);
    Optional<UserEntity> findByLogin(String login);

}
