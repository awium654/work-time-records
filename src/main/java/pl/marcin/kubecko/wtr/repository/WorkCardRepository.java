package pl.marcin.kubecko.wtr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.marcin.kubecko.wtr.entity.WorkCard;

import java.util.List;
import java.util.Optional;

public interface WorkCardRepository extends JpaRepository<WorkCard, Integer> {

    Optional<WorkCard> findByIdAndUserId(Integer id, Integer userId);
    List<WorkCard> findByUserId(Integer userId);
    List<WorkCard> findByUserIdAndNameWorkContainsIgnoreCaseOrUserIdAndNameActionContainsIgnoreCase(Integer userId, String nameWork, Integer userIdSecond, String nameAction);

}
