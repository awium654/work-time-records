package pl.marcin.kubecko.wtr.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.marcin.kubecko.wtr.dto.UserDTO;
import pl.marcin.kubecko.wtr.mapper.UserMapper;
import pl.marcin.kubecko.wtr.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MainController {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public String openMainPage() {
        logger.info("open index in mainController");
        return "index";
    }

    @GetMapping("/login")
    public String login(@RequestParam(required = false) boolean changePasswordIsTrue, Model model) {
        model.addAttribute("changePasswordIsTrue", changePasswordIsTrue);
        return "login";
    }
    @GetMapping("/getUsers/delete")
    public String viewAllUsersDelete(Model model) {
        List<UserDTO> findAllUserDTO = userRepository.findAll().stream().map(UserMapper::mapToUserDTO)
                .collect(Collectors.toList());
        model.addAttribute("allUsers",findAllUserDTO);
        return "viewAllUsersDelete";
    }
}
