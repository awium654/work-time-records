package pl.marcin.kubecko.wtr.dto;

import java.time.OffsetDateTime;

public class WorkCardDTO {

    private Integer id;
    private String timeStart;
    private String timeEnd;
    private String nameWork;
    private String nrMachine;
    private String nameAction;
    private Integer quantity;
    private Integer qntInMagazine;
    private Integer badArt;
    private Integer userId;
    private OffsetDateTime date_and_Time;

    public WorkCardDTO() {
    }

    public WorkCardDTO(Integer id, String timeStart, String timeEnd, String nameWork, String nrMachine,
                       String nameAction, Integer quantity, Integer qntInMagazine, Integer badArt, Integer userId,
                       OffsetDateTime date_and_Time) {
        this.id = id;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.nameWork = nameWork;
        this.nrMachine = nrMachine;
        this.nameAction = nameAction;
        this.quantity = quantity;
        this.qntInMagazine = qntInMagazine;
        this.badArt = badArt;
        this.userId = userId;
        this.date_and_Time = date_and_Time;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameWork() {
        return nameWork;
    }

    public void setNameWork(String nameWork) {
        this.nameWork = nameWork;
    }

    public String getNrMachine() {
        return nrMachine;
    }

    public void setNrMachine(String nrMachine) {
        this.nrMachine = nrMachine;
    }

    public String getNameAction() {
        return nameAction;
    }

    public void setNameAction(String nameAction) {
        this.nameAction = nameAction;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQntInMagazine() {
        return qntInMagazine;
    }

    public void setQntInMagazine(Integer qntInMagazine) {
        this.qntInMagazine = qntInMagazine;
    }

    public Integer getBadArt() {
        return badArt;
    }

    public void setBadArt(Integer badArt) {
        this.badArt = badArt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public OffsetDateTime getDate_and_Time() {
        return date_and_Time;
    }

    public void setDate_and_Time(OffsetDateTime date_and_Time) {
        this.date_and_Time = date_and_Time;
    }

    @Override
    public String toString() {
        return "nameWork='" + nameWork + '\'' +
                ", nrMachine='" + nrMachine + '\'' +
                ", nameAction='" + nameAction + '\'' +
                ", quantity=" + quantity +
                ", qntInMagazine=" + qntInMagazine +
                ", badArt=" + badArt +
                ", userId=" + userId +
                ", date_and_Time=" + date_and_Time;
    }
}
