package pl.marcin.kubecko.wtr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//public class WorkTimeRecordsApplication  extends SpringBootServletInitializer{
    public class WorkTimeRecordsApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorkTimeRecordsApplication.class, args);
    }

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(WorkTimeRecordsApplication.class);
//    }
}
