package pl.marcin.kubecko.wtr.dto;

public class ResetPasswordDTO {

    private String oldPassword;
    private String newPassword;

    public ResetPasswordDTO() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
