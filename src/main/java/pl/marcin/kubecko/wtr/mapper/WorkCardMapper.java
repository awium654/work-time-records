package pl.marcin.kubecko.wtr.mapper;

import org.springframework.stereotype.Component;
import pl.marcin.kubecko.wtr.dto.WorkCardDTO;
import pl.marcin.kubecko.wtr.entity.WorkCard;

@Component
public class WorkCardMapper {

    public static WorkCard mapToWorkCard(final WorkCardDTO workCardDTO) {
        return new WorkCard (workCardDTO.getId(),
                workCardDTO.getTimeStart(),
                workCardDTO.getTimeEnd(),
                workCardDTO.getNameWork(),
                workCardDTO.getNrMachine(),
                workCardDTO.getNameAction(),
                workCardDTO.getQuantity(),
                workCardDTO.getQntInMagazine(),
                workCardDTO.getBadArt(),
                workCardDTO.getUserId(),
                workCardDTO.getDate_and_Time());
    }

    public static WorkCardDTO mapToWorkCardDTO(final WorkCard workCard) {
        return new WorkCardDTO (workCard.getId(),
                workCard.getTimeStart(),
                workCard.getTimeEnd(),
                workCard.getNameWork(),
                workCard.getNrMachine(),
                workCard.getNameAction(),
                workCard.getQuantity(),
                workCard.getQntInMagazine(),
                workCard.getBadArt(),
                workCard.getUserId(),
                workCard.getDate_and_Time());
    }
}
